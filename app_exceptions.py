# -*- coding: utf-8 -*-
"""
Created on Sat Sep 11 13:25:35 2021

@author: russ



?? move to rshlib




"""





class ApplicationError( Exception ):   # custom exception
    def __init__(self, why, errors = "not given"):

        # Call the base class constructor with the parameters it needs
        super( ).__init__(   )
        self.why    = why
        # Now for your custom code...
        self.errors = errors



class ReturnToGui( ApplicationError ):   # custom exception
    def __init__(self, why, errors = "not given"):

        """
        when we want to return from any nested code and give control back to the gui
        consider placing a user message in message if we do not want
        it down where the exception is raised
        """

        # Call the base class constructor with the parameters it needs
        super( ).__init__(  why  )  # message

        # Now for your custom code...
        self.errors = errors
