# -*- coding: utf-8 -*-

"""
basic database operations and objects
but sort of messed up, probably needs re factoring

"""

import os
#import sys
import sqlite3 as lite


# ------------ local
from   app_global import AppGlobal


# ------------------ module functions
# -----------------------------------

#------------------------------------
class DBObjects( object ):
    """
    manages db access with just a bit of state, state could be in module
    ?? sort of but more
    """
    def __init__(self, db_file_name = None  ):

        self.db_file_name        = db_file_name    # error to use till set with # instance.db_file_name = "somefile.db"
        print( "DBObjects look for unimplemented functions " )
        AppGlobal.db_objects     = self
        self._connection         = None
        self.ix_batch            = 0
        self.max_ix_batch        = 10
        self.get_connection(   )
        self.commit( for_sure = True )

     # ----------------------------------------------
    # called by str( instance of AppClass )
    def get_connection( self ):

        if self.db_file_name is None:
            self.db_file_name  = "file::memory:?cache=shared" # ng ?
            self.db_file_name  = "file:cachedb?mode=memory&cache=shared"

            #However, the database will be erased w
        if self._connection is None:
            try:
                self._connection   =  lite.connect( self.db_file_name )
                print( f"create connection for  {self.db_file_name} {self._connection}" )

            except lite.Error as a_except:
                 print( type(a_except), '::', a_except )
                 print( f"unable to open: self.db_file_name = {self.db_file_name}")
        print( f"return connection {self._connection}")
        return  self._connection

    # ----------------------------------------------
    # called by str( instance of AppClass )
    def __str__(self):
        a_str      = f"DBObjects  db_file_name = {self.db_file_name}"
        return a_str


   #  # ----------------------------------------------
   #  def open( self, db_file_name ):
   #      """
   #      support ram as well as file
   #      """
   #      pass

   # # ----------------------------------------------
    def close( self,   ):
        """
        what it says, read
        """
        self._connection.close()

    # # ----------------------------------------------
    def commit( self,  for_sure = False ):
        """
        what it says, read
        """
        if for_sure:
            self.ix_batch           = 0

        else:
            self.ix_batch            += 1
            if self.ix_batch < self.max_ix_batch:
                # need final commit
                return

        self._connection.commit()

   # ----------------------------------------------
    def get_cursor( self,   ):
        """
        support ram as well as file
        remember to commit ?? close
        """
        cur = self._connection.cursor()
        return cur

    # ----------------------------------------------
    def define_table_dups( self, allow_drop = False ):
        """  ============= define_table_dups =====================
        return  -- none
        """

        cur   = self.get_cursor()

        try:

            if allow_drop:
               cur.execute("DROP TABLE IF EXISTS possible_file_dups")   # else error if table exists
            sql   =  ( "CREATE TABLE possible_file_dups ( "
                          "file_name Text, "
                          "path      Text, "            # path is the full file name path inc
                          "file_size INT ) "  )
            print( sql )

            cur.execute( sql )

        except lite.Error as a_except:
         #except ( lite.Error, TypeError) as a_except:
             print( type(a_except), '::', a_except )
             print( "error define_table_dups, exception {a_except}" )
             raise

        self.commit()

    # ----------------------------------------------
    def process_keepxxxxxx( self, file_info  ):
        """
        select and decide if dup is a dup or non-dup
        and take action, could include delete and moving to a new dir
        ?? pass file count in row data

        ---- to move
        extract dir from file
        get del dir   = dir + /duplicate
        see if dir exists and if not create
        move

        a_path       = Path( full_file_name ).parent.absolute()
        a_path       = f"{a_path}/{AppGlobal.parameters.del_directory}


        """
        #rint( f"process_keep row_data -> {row_data}" )
        file_name        = file_info.file_name
        full_file_name   = file_info.full_file_name

        full_file_name   = full_file_name.replace( "\\", "/" )   # normalize \ /  ?? already done ??

        #rint( f"process_keep row_data -> {row_data}" )
        dup_files   = self.select_from_dups( [file_name] )

        #rint( f"for {file_name} found -> {dup_files}")

        for i_file_dup in dup_files:

            i_file_name_dup  = i_file_dup[1]
            #rint( f"delete if dup{i_file_name_dup}")
            i_file_name_dup   = i_file_name_dup.replace( "\\", "/" )   # perhaps do in db too !!

            if i_file_name_dup == full_file_name:
                msg    = f"keep and dup == {i_file_name_dup} no deletion"
                print(  msg )
                AppGlobal.controller.display_in_gui( msg )
            else:

                if not AppGlobal.parameters.simulate:
                    msg    = f"deleting {i_file_name_dup}"
                    print(  msg )
                    AppGlobal.controller.display_in_gui( msg )
                    AppGlobal.logger.debug( msg )
                    self.remove_file( i_file_name_dup )
                else:
                    msg    = f"simulate deleting {i_file_name_dup}"
                    print(  msg )
                    AppGlobal.controller.display_in_gui( msg )
                    AppGlobal.logger.debug( msg )

    # ----------------------------------------------
    def remove_file( self, file_name,  ):
        """
        remove/delete a file
        file_name includes the path
        include some logging
        """
        if os.path.isfile( file_name ):

            try:
                os.remove( file_name )                   # error if file not found
                print(f"removed {file_name} " )
                # could log ??
            except OSError as error:
                print( error )
                print( f"os.remove threw error on file {file_name }")
                # need loging !!
        else:
            print( f"file already gone  {file_name}")
            # need loging !! gui....

    # ----------------------------------------------
    def select_from_dups( self, file_info,  ):
        """
        ( file_info.file_name, file_info.full_file_name )  file_info as arg ??
        find rows of files that are dups
        return list of lists  in list we have file_name, full_file_name size and maybe more
        see code
        """
        file_name  = file_info.file_name
        #self.start_time     = time.time()
        #rint( f"select_from_dups args file_name -> {file_name} ")
        sql         = "SELECT file_name, path FROM possible_file_dups WHERE file_name = ?"

        #print( f"sql -> {sql}")
        sql_data    = [file_name]
        ret_files   = []
        cur         = self.get_cursor()

        try:
                execute_args  = ( sql,  sql_data,  )
                msg           = f"select and output; execute_args {execute_args}"
                #rint(  msg )
                AppGlobal.logger.debug( msg )
                cur.execute( *execute_args )

                while True:  # get rows one at a time in loop
                    row   = cur.fetchone()
                    if row is None: # end of select
                         break
                    row  = list( row )
                    ret_files.append( row )

        except lite.Error as a_except:
         #except ( lite.Error, TypeError) as a_except:
             print( type(a_except), '::', a_except )
             print( "error select_from_dups, exception {a_except}" )
             raise

        self.commit()
        return ret_files

    # ----------------------------------------------
    def write_file_dups( self, file_name,  ):
        """
        ( file_info.file_name, file_info.full_file_name )  file_info as arg ??
        find rows of files that are dups
        return list of lists  in list we have file_name, full_file_name size and maybe more
        see code

        """
        #file_name  = file_info.file_name
        #self.start_time     = time.time()
        #rint( f"select_from_dups args file_name -> {file_name} ")
        print( f"write_file_dups, file_name {file_name}" )

        with open( file_name, 'a') as a_file:    # a will append so file should be deleted time to time "w" will be for write no append ?

            # !! need an order by
            sql         = "SELECT path FROM possible_file_dups "

            #print( f"sql -> {sql}")

            ret_files   = []
            try:

                    cur           = self.get_cursor()
                    execute_args  = ( sql,     )
                    msg           = f"select and output; execute_args {execute_args}"
                    #rint(  msg )
                    AppGlobal.logger.debug( msg )
                    print( msg )
                    cur.execute( *execute_args )

                    while True:  # get rows one at a time in loop
                        row   = cur.fetchone()
                        if row is None: # end of select
                             break
                        row  = list( row )
                        ret_files.append( row )
                        print( "better to output here", row )
                    print( "select complete ")
                    self.commit()


            except lite.Error as a_except:
             #except ( lite.Error, TypeError) as a_except:
                 print( type(a_except), '::', a_except )
                 print( "error select_from_dups, exception {a_except}" )
                 raise
                         # first time through the list
            for i_line in ret_files:
                a_file.write( f"{i_line[0]}\n" )


        return ret_files

    # ----------------------------------------------
    def insert_dup( self, file_info  ):
        """
        FileInfo( "file_name = i_name, "path_name = starting_dir, full_file_name = i_full_name )
        insert possible dup file into the db
        write output
        execute many list of list or since one at a time, for now just execute
        row_data must match values....

         self.ix_batch            = 0
        self.max_ix_batch        = 10
        need final commit


        """
        sql  = ( "INSERT INTO possible_file_dups "
                             " (  file_name, path, file_size     ) VALUES  "
                             " (          ?,    ?,    ?     )" )
        #rint( sql )
        #rint( f"insert --> {row_data}" )
        try:

                cur           = self.get_cursor()

                #cur.executemany( sql, row_data )
                cur.execute( sql, [ file_info.file_name, file_info.full_file_name, 0 ] )

                self.commit()
                # sql_con.close()   # with takes care of this ?

        except Exception as a_except:  # !! be more specific
                print( "exception in insert_dup"    )
                print( type(a_except), '>>', a_except )
                raise

# ----------------------------------------------
if __name__ == "__main__":
    import  file_finder
    file_finder.main( )

# =================== eof ==============================













