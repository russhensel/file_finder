# -*- coding: utf-8 -*-
"""
Created on Sat Jul  3 11:00:31 2021

@author: russ
"""

import os
import shutil

import helper_thread
from   app_global import AppGlobal


import parameters


# --------------------------------------
class ExploreArgs( object ):
    """
    args passed down recursive explore functions, treat as constants once set
    may want a copy for each thread and possibly accumulate data here

    also functions used for processing  .... perhaps a rename ??
    but some args are functions, here or in helper_thread ??

    """
    #---------------------
    def __init__( self, db_objects ):
        """
        Usual init see class doc string
        """
        self.ff                = None
        self.df                = None
        self.process_item      = None    # function to process item need common args
        self.max_dir_depth     = None

        self.process_item_fun  = None   #  keeps and dups
        self.process_file_fun  = None  # used with process keeps only

        self.db_objects        = db_objects

    #------------------------------------------
    def set_for_keep( self,   ):
        """
        parameters for exploring the keep dir tree
        filter could be here or in   _keeps or both

        """
        self.ff               = AppGlobal.parameters.file_filter_object.check_file
        self.df               = AppGlobal.parameters.dir_filter_object.check_dir     # for directories


        self.max_dir_depth    = AppGlobal.parameters.keep_dir_depth

        self.process_item_fun = self.process_keeps

        parameters   = AppGlobal.parameters

        if  parameters.dup_mode == "delete":
            self.process_file_fun =   self.process_file_delete
                          #
        elif parameters.dup_mode == "move":
            self.process_file_fun = self.process_file_move

        else:   # "simulate"
            self.process_file_fun =   self.process_file_simulate

     #------------------------------------------
    def set_for_dups( self,   ):
        """
        parameters for exploring the dups dir tree
        filter could be here or in   _keeps or both now both --- reduces processing

        """
        self.ff               = AppGlobal.parameters.file_filter_object.check_file
        self.df               = AppGlobal.parameters.dir_filter_object.check_dir     # for directories

        self.max_dir_depth    = AppGlobal.parameters.dups_dir_depth

        self.process_item_fun     = self.process_dups  # see set for_dups, set for keyps
                                  # a_db_objects.process_keep  # function to process item need common args
        self.process_file_fun =   None  # used with process keeps only

    # functions that are used when set up for keeps, thes are the actions
    # ----------------------------------------------
    def process_file_delete( self, file_info ):
        """
        action when a keep file locates a duplicat file to take action

        """
        msg    = f"deleting {file_info.full_file_name}"
        print(  msg )
        print( file_info )
        AppGlobal.controller.display_in_gui( msg )
        AppGlobal.logger.debug( msg )

        self.db_objects.remove_file( file_info.full_file_name )

    # ----------------------------------------------
    def process_file_move( self, file_info ):
        """
       action when a keep file locates a duplicate file to take action

        ---- to move
        extract dir from file
        get del dir   = dir + /duplicate
        see if dir exists and if not create
        move

        a_path       = Path( full_file_name ).parent.absolute()
        a_path       = f"{a_path}/{AppGlobal.parameters.del_directory}
        """
        pass
        #a_path       = Path( full_file_name ).parent.absolute()
        a_path       = file_info.path_name
        a_path       = f"{a_path}/{AppGlobal.parameters.del_directory}"
        if not os.path.isdir( a_path ):
            try:
               os.mkdir( a_path )
            except:
                msg = "making dir {a_path} threw exception"
                print(  msg )
                AppGlobal.controller.display_in_gui( msg )
                AppGlobal.logger.error( msg )
                raise

        # now move the file

        dst     = a_path + "/" +  file_info.file_name
        src     = file_info.full_file_name
        #rint( "move>>>>", src, dst )
        shutil.move( src, dst, )   # copy_function=copy2

    # ----------------------------------------------
    def process_file_simulate( self, file_info ):
        """
        action when a keep file locates a duplicat file to take action

        """
        full_file_name    = file_info.full_file_name
        msg    = f"simulate deleting {full_file_name}"
        print(  msg )
        AppGlobal.controller.display_in_gui( msg )
        AppGlobal.logger.debug( msg )

    # ----------------------------------------------
    def process_keeps( self, file_info ):
        """
        this is how we process a keep file -- look for the dups and take action
        called by explore_dir_keeps or explore_dir

        """
        full_file_name  = file_info.full_file_name
        file_name       = file_info.file_name

        dup_files   = self.db_objects.select_from_dups( file_info )   # [] because needs list

        #rint( f"for {file_name} found -> {dup_files}")

        for i_file_dup in dup_files:

            i_full_file_name_dup  = i_file_dup[ 1  ]        # this is not a named tuple
            #rint( f"delete if dup{i_file_name_dup}")
            i_full_file_name_dup   = i_full_file_name_dup.replace( "\\", "/" )   # perhaps do in db too !!

            if i_full_file_name_dup == full_file_name:
                msg    = f"keep and dup == {i_full_file_name_dup} no deletion"
                print(  msg )
                AppGlobal.controller.display_in_gui( msg )
            else:

                path_name_dup   =  os.path.dirname(  i_full_file_name_dup )            # need to split off path
                file_info_dup   = helper_thread.FileInfo( file_name = file_name, path_name = path_name_dup, full_file_name = i_full_file_name_dup )

                self.process_file_fun( file_info_dup )   # this needs to be the dup file -- check for correct

    # ----------------------------------------------
    def process_dups( self, file_info ):
       """
       process the dups by adding to the db

       """
       #rint( "process_dups" )
       self.db_objects.insert_dup( file_info  )
        #rint( f"for {file_name} found -> {dup_files}")

# =================================================
if __name__ == "__main__":

    import  file_finder
    file_finder.main( )

# =================== eof ==============================

