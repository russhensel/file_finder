# -*- coding: utf-8 -*-
#! /usr/bin/python3
#!python3
# above for windows ??
#     /usr/bin/python

#print( "start imports" )  # debugging splash screen

"""
*>text

"""

import os
import logging
import sys
import importlib
import time
import datetime
import queue


#----- local imports

# I normally do not install the backup app, so I need to point to code, you will need to adjust for your system
# adapts to this computer
sys.path.append( "D:/Russ/0000/python00/python3/_projects/backup" )
sys.path.append( "D:/Russ/0000/python00/python3/_projects/rshlib" )

import parameters
import gui
from   app_global import AppGlobal
#import file_filters
import helper_thread
import app_state


# ------------------------------
def   print_uni( a_string_ish ):
    """
    print even if unicode char messes it conversion
    maybe doing as a call is over kill
    """
    print(  a_string_ish.encode( 'ascii', 'ignore') )


# ============================================
class App( object ):
    """
    this class is the "main" or controller for the whole app
    to run see end of this file
    it is the controller of an mvc app
     """
    def __init__( self, ):
        """
        usual init for main app
        """
        self.app_name          = "FileFinder"
        self.app_version       = "Ver 9:  2021_17_13.01"
        self.version           = self.app_version        # hack till later
        AppGlobal.controller   = self
        self.gui               = None
        self.sort_list         = None   # create later may not be a list

        self.logger            = None   # for access in rest of class?
        AppGlobal.logger       = None

    # ----------------------------------
    def restart(self ):
        """
        use to restart the app without ending it
        this can be very quick
        """
        print( "========= restart file_finder =================" ) # not logged until it is turned on
        if not( self.gui is None ):
            self.gui.root.destroy()
            importlib.reload( parameters )    # should work on python 3 but sometimes does not

        else:
            #self.q_to_splash
            pass

        self.parameters     = parameters.Parameters( ) # open early as may effect other parts of code

        self.config_logger()
        self.prog_info()

        self.gui      = gui.GUI( self )

        msg           = "Error messages may be in log file, check it if problems -- check parmeters.py for logging level "
        print( msg )
        AppGlobal.print_debug( msg )
        self.logger.log( AppGlobal.fll, msg )

        self.app_state        = app_state.AppState()

        # set up queues before creating helper thread
        self.queue_to_helper    = queue.Queue( self.parameters.queue_length )   # send strings back to tkinker mainloop here
        self.queue_fr_helper    = queue.Queue( self.parameters.queue_length )
        #self.helper_thread      = smart_plug_helper.HelperThread( )
        self.helper_thread      = helper_thread.HelperThread()

        #AppGlobal.helper        = self.helper_thread  move to helper_thread
        self._polling_fail      = False   # flag set if _polling in gui thread fails

        self.helper_thread.start()

        self.polling_delta  = self.parameters.gt_poll_delta_t

        self.starting_dir   = os.getcwd()    # or perhaps parse out of command line
        self.gui.root.after( self.polling_delta, self._polling_0_ )

#        print( "all done kernal error? return ")
#        return
        #Make the window jump above all
        self.gui.root.attributes( '-topmost', True  )
        self.gui.root.attributes( '-topmost', False )
        self.gui.root.mainloop()

        self.post_to_queue( "stop", None  , (  ) )

        self.helper_thread.join()
        self.logger.info( self.app_name + ": all done" )

    # ------------------------------------------
    def config_logger( self, ):
        """
        create/configure the python logger
        """
        AppGlobal.logger_id     = "App"        # or prerhaps self.__class__.__name__
        logger                  = logging.getLogger( AppGlobal.logger_id )
        logger.handlers         = []

        logger.setLevel( self.parameters.logging_level )

        # create the logging file handler
        fh = logging.FileHandler( self.parameters.pylogging_fn )

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        logger.addHandler(fh)

        logger.info( "Done config_logger .. next AppGlobal msg" )
        #rint( "configed logger", flush = True )
        self.logger      = logger   # for access in rest of class?
        AppGlobal.logger = logger

        msg  = f"Message from AppGlobal.print_debug >> logger level in App = {self.logger.level} will show at level 10"
        AppGlobal.print_debug( msg )

    # --------------------------------------------
    def prog_info( self,  ):
        """
        ?? consider gui display or with button
        """
        #logger_level( "util_foo.prog_info"  )
        fll         = AppGlobal.force_log_level
        logger      = self.logger
        logger.log( fll, "" )
        logger.log( fll, "============================" )
        logger.log( fll, "" )
        title       =   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        logger.log( fll, title )
        logger.log( fll, "" )

        if len( sys.argv ) == 0:
            logger.info( "no command line arg " )
        else:

            for ix_arg, i_arg in enumerate( sys.argv ):
                msg = f"command line arg + {str( ix_arg ) }  =  { i_arg })"
                logger.log( AppGlobal.force_log_level, msg )

        logger.log( fll, f"current directory {os.getcwd()}"  )

        start_ts     = time.time()
        dt_obj       = datetime.datetime.utcfromtimestamp( start_ts )
        string_rep   = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        logger.log( fll, "Time now: " + string_rep )
        # logger_level( "Parameters say log to: " + self.parameters.pylogging_fn ) parameters and controller not available can ge fro logger_level

        return

    #----------------------------------------------------
    def process_dup_file( self,  ):
        """
        read in dup file line by line and process, a line() at the top might be
        good to indicate purpose of file -- validate
        """
        msg    = "process_dup_file..."
        self.logger.info( msg )
        self.gui.write_gui( msg )

        dir_list   = self.parameters.dir_list
        dir_ok     = self.check_dir_list( dir_list )
        if dir_ok != "":
            msg    = dir_ok
            self.logger.info( msg )
            self.gui.write_gui( msg + "\n" )
            return   # bad ending

        # for now do not check type of processing or file header
        my_errors       = 0
        process_type    = None

        with open( self.parameters.file_dup_list_fn ) as f:  # look at slerp 2 and use comphrension
            for i_line in f:
                i_line       = i_line.rstrip('\n')
                this_item    = i_line.split( "\t" )
                do_what      = this_item[0]
                to_what      = this_item[2]

                msg          = f"{do_what} to {to_what}"
                self.logger.info( msg )

                if  do_what  == "keep":
                    keep_file   = to_what
                    if os.file.exists( keep_file ):
                        print( f"file ok >>{keep_file}<<" )
                    else:
                        msg    = f"error file >>{keep_file}<< not found process dup_file_terminated early"
                        self.logger.info( msg )
                        self.gui.write_gui( msg )
                        return

                elif do_what == "delete":
                    delete_file  = to_what
                    pass
                else:
                    pass
                # receck dup ?...
        msg    = "process_dup_file done\n"
        self.logger.info( msg )
        self.gui.write_gui( msg )

# ---------------------- Helper Methods
#----------------------------------------------------
    def check_dir_list( self, dir_list ):
        """
        make sure all directories exist
        error returned in ret no logging
        """
        max_dirs  = 9        # match to sort function str padding or whatever  why this or not in parms

        if len( dir_list ) > max_dirs:
            ret = f"error number of directories greater than {max_dirs} dir.list >>{dir_list}<<"
            return ret

        for  i_dir in dir_list:
            if os.path.exists( i_dir ):
            #if i_dir.is_dir():  # not on a string
                msg  = f"dir ok >>{i_dir}<<"
                self.logger.info( msg )
                self.gui.write_gui( msg + "\n" )

            else:
                ret    = f"error dir >>{i_dir}<< not found"
                return ret  # error return no further checking
        return "" # "" is ok

    # ----------------------------------
    def _polling_0_( self,  ):
        """
        poll for clipboard
        this is for one itteration
        """
        pass
        self._polling_()

    # ----------------------------------
    def _polling_( self,  ):
        """
        poll for clipboard change and process themn
        protect with a try so polling is not crashed -- "no matter what"
        """
        gt_poll_delta_t   = self.parameters.gt_poll_delta_t
        try:   # this is for talking with the second thread
            ( action, function, function_args ) = self._rec_from_queue_()
            while action != "":
                if action == "call":
                    #rint( "controller making call" )
                    sys.stdout.flush()
                    function( *function_args )
                elif action == "rec":
                    self.gui.print_rec_string(  function_args[ 0 ] )
                elif action == "send":
                    # but where is it actually sent ??
                    self.gui.print_send_string( function_args[ 0 ] )
                elif action == "info":
                    self.gui.print_info_string( function_args[ 0 ] )

                ( action, function, function_args ) = self._rec_from_queue_()

        except Exception as ex_arg:
            self.logger.error( "_polling Exception in file_finder: " + str( ex_arg ) )
            # ?? need to look at which we catch maybe just rsh
            (atype, avalue, atraceback)   = sys.exc_info()
            a_join  = "".join( traceback.format_list ( traceback.extract_tb( atraceback ) ) )  # !! fix error
            self.logger.error( a_join )
            raise    # comment out

        finally:
            if  self._polling_fail:
                pass
            else:
                self.gui.root.after( gt_poll_delta_t, self._polling_ )  # reschedule event
                #self.graph_live.polling_mt()
        #rint("helper polling")
        #ime.sleep( 1 )             # debug
        return

  # --------------------------------------------------
    def post_to_queue( self, action, function, args ):
        """
        self.post_to_queue( action, function, args )
        request action by other thread
        """
        loop_flag          = True
        ix_queue_max       = 10
        ix_queue           = 0
        while loop_flag:
            loop_flag      = False
            ix_queue  += 1
            try:
                #rint( f"try posting {( action, function, args )}" )
                self.queue_to_helper.put_nowait( ( action, function, args ) )
            except queue.Full:

                # try again but give _polling a chance to catch up
                msg   = "smart_plug queue to ht full looping"
                print(  msg )
                self.logger.error( msg )
                # protect against infinite loop if queue is not emptied
                if self.ix_queue > ix_queue_max:
                    msg   = f"too much queue looping self.parameters.queue_sleep {self.parameters.queue_sleep}"
                    print( msg )
                    self.logger.error( msg )
                    pass
                else:
                    loop_flag   = True
                    time.sleep( self.parameters.queue_sleep )

    # --------------------------------------------------
    def _rec_from_queue_( self, ):
        """
        take an item off the queue, think here for expansion may not be currently used.
        ( action, function, function_args ) = self._rec_from_queue_()
        """
        try:
            action, function, function_args   = self.queue_fr_helper.get_nowait()
        except queue.Empty:
            action          = ""
            function        = None
            function_args   = None

        return ( action, function, function_args )

    #----------------- bcb's ----------------------
     # ----------------------------------
    def bcb_define_db( self,  ):
        """
        define a db
        """
        self.gui.do_clear_button( None )
        msg   = "---- Define Empty DB ----"
        self.display_in_gui( msg )

        self.gui.enable_ht_buttons( False )

        a_function   = self.helper_thread.define_table_dups
        self.post_to_queue( "call", a_function, ( "",  ) )

    # ----------------------------------------------
    def bcb_explore_dups( self,  ):
        """
        used as callback from gui button
        """
        self.gui.do_clear_button( None )
        msg   = "---- Explore (possible) Duplicate Files ----"
        self.display_in_gui( msg )

        self.gui.enable_ht_buttons( False )

        a_function   = self.helper_thread.explore_dir_dups
        self.post_to_queue( "call", a_function, ( self.parameters.dup_dir_list,  ) )

    # ----------------------------------------------
    def bcb_write_file_dups( self,  ):
        """
        used as callback from gui button
        """
        #self.helper_thread.db_objects.write_file_dups( "file_name.txt" )
        # msg   = "---- bcb_write_file_dups complete ----"
        # self.display_in_gui( msg )

        a_function   = self.helper_thread.write_file_dups
        self.post_to_queue( "call", a_function, (    ) )

     # ----------------------------------------------
    def bcb_explore_keeps( self,  ):
        """
        used as callback from gui button
        """
        self.gui.do_clear_button( None )
        msg   = "---- Explore Directories to Keep Files ----"
        self.display_in_gui( msg )
        self.gui.enable_ht_buttons( False )
        # self.helper_thread.explore_dir_keeps( self.parameters.keep_dir_list,  )

        a_function   = self.helper_thread.explore_dir_keeps
        self.post_to_queue( "call", a_function, ( self.parameters.keep_dir_list,  ) )

    # ----------------------------------------------
    def bcb_stop( self,  ):
        """
        used as callback from gui button
        stop ht if running
        """
        #rint( "bcb_stop" )
        self.app_state.cancel_flag  = True

    # ----------------------------------------------
    def bcb_pause( self,  ):
        """
        used as callback from gui button
        pause the ht
        """
        #rint( "bcb_pause" )
        if self.app_state.pause_flag:
            new_state = False
            bn_text   = "Pause"
        else:
            new_state = True
            bn_text   = "Continue"

        self.app_state.pause_flag  = new_state
        self.gui.widget_pause.config( text  = bn_text  )

    # ----------------------------------------------
    def bcb_show_setup( self,  ):
        """
        used as callback from gui button  -- use with a make string ??
        what it says, read code
        """
        #rint( "bcb_show_setup" )

        self.gui.do_clear_button( None )
        msg   = "---- Show Setup ----"
        self.display_in_gui( msg )

        # -----
        msg   = f"\nMode:  {self.parameters.mode} --- Dup mode = {self.parameters.dup_mode}"
        self.display_in_gui( msg )

        # ------------- filters ------------
        msg    = f"File Filter Info {AppGlobal.parameters.file_filter_object} "
        self.display_in_gui( msg )

        # ----- keep
        msg   = "\nKeep Directories:"
        self.display_in_gui( msg )

        self.display_list_in_gui( self.parameters.keep_dir_list  )

        self.display_in_gui(  f"    Keep Directory depth -> {self.parameters.keep_dir_depth}" )

        # ---- dups
        msg   = "\nDup Directories:"
        self.display_in_gui( msg )

        self.display_list_in_gui( self.parameters.dup_dir_list  )
        self.display_in_gui(  f"    Dup Directory depth -> {self.parameters.dup_dir_depth}" )

        self.display_in_gui(  f"\nDebug Level -> {self.parameters.logging_level}" )

        msg    = f"db_file_name: {self.parameters.db_file_name}"
        self.display_in_gui( msg )

        msg   = "\n---- Show Setup Done----"
        self.display_in_gui( msg )

   # ----------------------------------------------
    def display_list_in_gui( self, a_list ):
        """
        so short inline it ??
        """
        for i_line in a_list:
            self.display_in_gui( f"    {i_line}"  )

    # ----------------------------------------------
    def bcb_test( self,  ):
        """
        used as callback from gui button
        test method only in development
        """
        print( "bcb_test" )

        # a_explore_args     = explore_args.ExploreArgs()
        # a_explore_args.set_for_keep()
        # self.helper_thread.explore_dir( r"D:\Russ\0000\python00\python3\_projects\clipboard", 0, a_explore_args )

        # db_file_name   = self.parameters.db_file_name
        # ret_file_data  = db_objects.select_from_dups( db_file_name, "b")
        # print( ret_file_data )

        # a_function   = AppGlobal.helper.print_ten_times
        # self.post_to_queue( "call", a_function, (  ) )

        # self.gui.enable_ht_buttons( False )
        # time.sleep( 3 )
        self.gui.enable_ht_buttons( False )

    # ----------------------------------------------
    def ht_state_change( self, is_busy  ):
        """
        used as callback from gui button resolve deleted methods
        """
        if is_busy:
            pass #
            print( "ht_state_change -- may need code ")  # ??
        else:
            print( "ht_state_change -- enable true, is busy false  ")
            self.gui.enable_ht_buttons( True )

    # ----------------------------------------------
    def null_method( self,  ):
        """
        used as callback from gui button resolve deleted methods
        """
        print("call to null_method")

    # ----------------------------------------------
    def os_open_logfile( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( self.parameters.pylogging_fn )

    # ----------------------------------------------
    def os_open_parmfile( self,  ):
        """
        used as callback from gui button
        """
        a_filename = self.starting_dir  + os.path.sep + "parameters.py"
        AppGlobal.os_open_txt_file( a_filename )

    # ----------------------------------------------
    def os_open_help( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_help_file( self.parameters.help_fn )

    # ----------------------------------------------
    def display_in_gui( self, a_string ):
        """
        what it says
        for prompt display need to have multi threading
        """
        self.gui.write_gui( a_string )

# ----------------------------------------------
def main():
    a_app = App( )
    #sg   = "init done but no logger"
    #rint( msg )
    if True:
        a_app.restart()
    else:

        try:
            a_app.restart()
        except Exception as exception:
            if a_app.logger == None:
                msg   = "exception in __main__ run the app -- it will end -- logger still None "
                print( msg )
            else:
                msg   = "exception in __main__ run the app -- it will end -- see logger "
                print( msg )
                a_app.logger.critical( msg )
                a_app.logger.critical( exception,  stack_info=True )   # just where I am full trace back most info
                raise
        finally:
            print( "here we are done with file_finder ", flush = True )
    #        sys.stdout.flush()

# ------------------
# ==============================================
if __name__ == '__main__':
    """
    run the app here, for convenience of launching
    """
    main(  )

# ======================= eof =======================

