# -*- coding: utf-8 -*-

#   gui for file_finder

import sys
import tkinter as Tk
import logging
import pyperclip

# ---- local
sys.path.append( "../rshlib" )
import gui_ext
#import gui_helper
from   app_global import AppGlobal


#----------------------------------------------------------------------
class RedirectText(object):
    """
    simple class to let us redirect console prints to our recieve area
    http://www.blog.pythonlibrary.org/2014/07/14/tkinter-redirecting-stdout-stderr/
    """
    #----------------------------------------------------------------------
    def __init__(self, text_ctrl):
        """Constructor
        text_ctrl text area where we want output to go
        """
        self.output = text_ctrl

    #----------------------------------------------------------------------
    def write(self, string):
        """
        """
        self.output.insert( Tk.END, string )
        # add scrolling?
        self.output.see( Tk.END )

        #self.rec_text.insert( END, adata, )
        #self.rec_text.see( END )

    #--------------------------------
    def flush(self, ):
        """
        here to mimic the standard sysout
        does not really do the flush
        """
        pass

# --------------------------------------
class TitledFrame( object ):
    """
    About this class.....
    make a color coded frame ( two frames one above the other ) with a title in the top one and color coded
    see ex_tk_frame.py for the master
    """
    #----------- init -----------
    def __init__( self, a_parent_frame, a_title, a_title_color, button_width = 10,  button_height = 2 ):
        """
        Usual init see class doc string
        add to gui_ext !!
        """
        a_frame      = Tk.Frame( a_parent_frame,  bg ="red", )

        a_frame.rowconfigure(    0, weight= 1 )
        a_frame.rowconfigure(    1, weight= 1 )

        a_frame.columnconfigure( 0, weight= 1 )
        #master.columnconfigure( 1, weight= 1 )
        self.frame      = a_frame
        p_frame         = a_frame

        a_frame  = Tk.Frame( p_frame,   bg = a_title_color, )      # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 0,  column = 0 ,sticky = Tk.E + Tk.W ) #+ Tk.N + Tk.S, )
        self.top_inner_frame    = a_frame

        a_label             = Tk.Label( a_frame, text = a_title, bg = a_title_color ,  ) #   relief = RAISED,  )
        a_label.grid( row = 0, column = 0, ) # columnspan = 1, sticky = Tk.W + Tk.E )

        a_frame  = Tk.Frame( p_frame,   ) # bg = "blue", )  # use neutral color or the title color
                                  # padx = 2, pady = 2, relief= Tk.GROOVE, )
        a_frame.grid( row = 1,  column = 0,sticky = Tk.E + Tk.W )   # + Tk.N + Tk.S, )
        self.bottom_inner_frame    = a_frame

        self.button_width  = button_width
        self.button_height = button_height
        self.button_row    = 0
        self.button_column = 0

        return

    #----------------------------------------------------------------------
    def make_button( self, button_text = "default text", command = None ):
        """
        !! have function make the button with the command
        """
        a_button = Tk.Button( self.bottom_inner_frame , width = self.button_width, height = self.button_height, text = button_text )
        a_button.grid( row  = self.button_row, column = self.button_column )
        self.button_column += 1

        if command is not None:
            a_button.config( command = command  )

        return a_button

# --------------------------------------

class GUI( object ):

    def __init__( self, controller  ):

        AppGlobal.gui       = self
        self.controller     = controller
        self.parameters     = controller.parameters   # or from app global
        self.logger         = logging.getLogger( AppGlobal.logger_id + ".gui")
        #self.logger.info("in class GUI init for the clip_board_1") # logger not currently used by here

        # ----- start building gui
        self.root           = Tk.Tk()

        #self.root.title( self.controller.parameters.title )   f"Application: {self.app_name} {AppGlobal.parameters.mode}  {self.app_version}"
        self.root.title( f"{self.controller.app_name} mode: {AppGlobal.parameters.mode} / version: {self.controller.app_version}" )

        #        # ----- set up root for resizing
        #        self.root.grid_rowconfigure(    0, weight=0 )
        #        self.root.grid_columnconfigure( 0, weight=1 )
        #
        #        self.root.grid_rowconfigure(    1, weight=1 )
        #        self.root.grid_columnconfigure( 1, weight=1 )


        # ------------ message frame ---------------------   rec
        self.cb_scroll_var   = Tk.IntVar()  #
        self.max_lines       = self.parameters.max_lines

        parent_frame         = self.root

        parent_frame.rowconfigure(    0, weight= 0 )
        parent_frame.rowconfigure(    1, weight= 0 )

        parent_frame.columnconfigure( 0, weight= 1 )

        parent_frame.configure( background="grey" )

        ix_row   = -1  # increment to 0 later
        # ----- ._make_meta_controls_frame
        a_frame   = self._make_meta_controls_frame( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----- _make_command_frame_
        a_frame   = self._make_command_frame_( self.root,  )
        ix_row   += 1
        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # was the id frame
#        a_frame   = Tk.Frame( self.root, width=300, height=20, bg = self.parameters.id_color , relief = Tk.RAISED, ) # borderwidth=1 )
#        ix_row   += 1
#        a_frame.grid( row=ix_row, column=0, columnspan = 2, sticky=Tk.N + Tk.S + Tk.E + Tk.W )

        # ----------- next frame
        ix_row   += 1
        frame0    = self._make_message_frame( self.root )
        frame0.grid( row=ix_row, column=0, sticky=Tk.N + Tk.S + Tk.E + Tk.W )
        self.root.grid_rowconfigure(    ix_row, weight=1 )

        # ------ end of frames

        self.root.geometry( self.controller.parameters.win_geometry )
        print( "next icon..." )

        if self.parameters.os_win:
            # icon may cause problem in linux for now only use in win
#            Changing the application and taskbar icon - Python/Tkinter - Stack Overflow
#            https://stackoverflow.com/questions/14900510/changing-the-application-and-taskbar-icon-python-tkinter
            import ctypes
            myappid = self.parameters.icon # arbitrary string
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)
#            print( "in windows setting icon" )
            self.root.iconbitmap( self.parameters.icon )
        msg       = "... icon done...."
        print( msg )

    # ------------------------------------------
    def _make_meta_controls_frame( self, parent_frame,  ):
        """
        this contains a button area for app control stuff
        passed a parent
        returns this main_frame
        """
        tframe_1      = TitledFrame( parent_frame, "System Operations:", self.parameters.id_color )
        a_frame       = tframe_1.frame

        a_button = tframe_1.make_button( button_text = "Ed Log", command = self.controller.os_open_logfile  )
        #a_button.config( command = self.controller.os_open_logfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Parms" )
        a_button.config( command = self.controller.os_open_parmfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Log" )
        a_button.config( command = self.controller.os_open_logfile  )

        a_button = tframe_1.make_button( button_text =  "Ed Parms" )
        a_button.config( command = self.controller.os_open_parmfile  )

        a_button = tframe_1.make_button( button_text =  "Restart" )
        a_button.config( command = self.controller.restart  )

        a_button = tframe_1.make_button( button_text =  "Help" )
        a_button.config( command = self.controller.os_open_help  )

        a_button = tframe_1.make_button( button_text =  "About" )
        a_button.config( command = AppGlobal.about  )

        return a_frame

    # ------------------------------------------
    def _make_command_frame_( self, parent_frame,  ):
        """
        buttons for commands that operate on files
        returns this frame
        !! have function make the button with the command
        """
        tframe_1      = TitledFrame( parent_frame, "File Operations:", self.parameters.id_color )
        a_frame       = tframe_1.frame

        a_widget = tframe_1.make_button( button_text = "0-Show\nSetup" )
        a_widget.config( command = self.controller.bcb_show_setup  )
        self.widget_define_db   = a_widget

        a_widget = tframe_1.make_button( button_text = "1-Define\nDB" )
        a_widget.config( command = self.controller.bcb_define_db  )
        self.widget_define_db   = a_widget

        a_widget = tframe_1.make_button( button_text = "2-Explore\nDups" )
        a_widget.config( command = self.controller.bcb_explore_dups  )
        self.widget_explore_dups  = a_widget

        a_widget = tframe_1.make_button( button_text = "2b-Write File\nDups" )
        a_widget.config( command = self.controller.bcb_write_file_dups  )
        self.widget_write_file_dups  = a_widget

        a_widget = tframe_1.make_button( button_text = "3-Explore\nKeeps" )
        a_widget.config( command = self.controller.bcb_explore_keeps )
        self.widget_explore_keeps  = a_widget

        a_widget = tframe_1.make_button( button_text = "Pause" )
        a_widget.config( command = self.controller.bcb_pause )
        self.widget_pause  = a_widget

        a_widget = tframe_1.make_button( button_text = "Stop" )
        a_widget.config( command = self.controller.bcb_stop )
        self.widget_stop  = a_widget

        a_widget = tframe_1.make_button( button_text = "Test" )
        a_widget.config( command = self.controller.bcb_test  )

        return a_frame

    # ------------------------------------------
    def _make_message_frame( self, parent,  ):
        """
        make the message frame for user feedback
        """
        a_frame              = gui_ext.MessageFrame( parent,  )
        self.message_frame   = a_frame
        return a_frame

    # ==================== end construction ====================
    # construction helpers

    def _make_titled_listbox_( self, parent_frame, a_title ):
        """
        return ( famelike_thing, listbox_thing)  ?? make a class, better acces to components
        """
        a_frame      = Tk.Frame(parent_frame)
        a_listbox    = Tk.Listbox( a_frame, height=5 )
        a_listbox.grid( column=0, row=1, sticky=(Tk.N, Tk.W, Tk.E, Tk.S) )
        s = Tk.Scrollbar( a_frame, orient=Tk.VERTICAL, command=a_listbox.yview)
        s.grid(column=1, row=1, sticky=(Tk.N, Tk.S))
        a_listbox['yscrollcommand'] = s.set
        a_label = Tk.Label( a_frame, text= a_title )
        a_label.grid( column=0, row=0, sticky=( Tk.N, Tk.E, Tk.W) )
        #  ttk.Sizegrip().grid(column=1, row=1, sticky=(Tk.S, Tk.E)) size grip not appropriate here
        a_frame.grid_columnconfigure( 0, weight=1 )
        a_frame.grid_rowconfigure(    0, weight=0 )
        a_frame.grid_rowconfigure(    1, weight=1 )
        return ( a_frame, a_listbox )

    #----------------------------------------------------------------------
    def write_gui_wt(self, title, a_string ):
        """
        write to gui with a title.
        title     the title
        a_string  additional stuff to write
        make a better function with title = ""  ??
        title the string with some extra formatting
        clear and write string to input area
        """
        self.write_gui( " =============== " + title  + " ==> \n" +  a_string )   # better format or join ??

    #----------------------------------------------------------------------
    def write_gui( self, string, add_nl = True ):
        """
        clear and write string to input area
        leave disabled
        """
        if add_nl:
            string   = string +"\n"
        self.message_frame.display_string( string )

    # ------------------------------------------
    def enable_ht_buttons( self, enable_if_true ):
        """
        for the clear button
        clear the text area
        even can be anything
        enable_if_true       True enable else disable
        """
        if enable_if_true:
            new_state  = Tk.NORMAL
            #rint( f"enable_ht_buttons Tk.NORMAL" )
        else:
            new_state  = Tk.DISABLED
            #rint( f"enable_ht_buttons Tk.DISABLED" )
        self.widget_define_db.config(        state = new_state )
        self.widget_explore_keeps.config(    state = new_state )
        self.widget_explore_dups.config(     state = new_state )
        self.widget_write_file_dups.config(  state = new_state )

    # ------------------------------------------
    def do_clear_button( self, event):
        """
        for the clear button
        clear the text area
        even can be anything
        """
        self.message_frame.do_clear_button( event )

    # ------------------------------------------
    def do_copy_button( self, event ):
        """
        copy all text to the clipboard
        """
        self.message_frame.do_copy_button()

    # ------------------------------------------
    def do_auto_scroll( self,  ):
        """
        pass, not needed, place holder
        """
        print( "do_auto_scroll needs completion" )
        # not going to involve controller
        pass
        return

# =================================================

if __name__ == "__main__":

    import  file_finder
    file_finder.main( )


# =================== eof ==============================