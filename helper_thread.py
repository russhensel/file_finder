# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 18:07:01 2021

@author: russ


copy and improve some code from backup

this helper_thread is for the file_finder app



#
#    !! limit on max depth may not be working
#
#    !! shut this down with an exception -- will this work, catch back in
#    !! the thread
#

"""

import stat
import time
import shutil
import os
import os.path
#import queue
import logging
#import datetime
#import sys
import traceback
#from pathlib import Path
#import pathlib
import time
import queue
import sys
#import datetime
import threading
import collections


# ------------- local imports
from   app_global import AppGlobal
import app_global

import db_objects
import explore_args

FileInfo   = collections.namedtuple( "FileInfo", "file_name path_name full_file_name" )


# ----------------------------------------------
class HelperThread( threading.Thread ):
    """
    this is the worker helper thread, as distinct from the gui thread
    """
    def __init__(    self, group=None,  target=None,   name=None, args=(), kwargs=None,  ):
        """
        call from main thread
        """
        threading.Thread.__init__( self, group=group, target=target, name=name, )   #

        AppGlobal.helper     = self    # regester

        # get for local ref, do we need?
        self.controller      = AppGlobal.controller
        self.parameters      = AppGlobal.parameters
        self.app_state       = AppGlobal.app_state
        #rint(  f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )
        self.logger          = logging.getLogger( f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )

        self.logger.info( "HelperThreadBackup backup_thread init")        # info debug...

        # self.db_objects                = db_objects.DBObjects( self.parameters.db_file_name )
        #self.db_objects.db_file_name   = self.parameters.db_file_name

        self.queue_to_helper        = self.controller.queue_to_helper
        self.queue_fr_helper        = self.controller.queue_fr_helper

        self.ht_poll_delta_t        = self.parameters.ht_poll_delta_t   # move to function !!

        self.delete_file_function   = None    # function for delete action set at beginning of expolore_dir_keep

        print( f"logger level in HelperThread = {self.logger.level}" )

    # ------------------------------------------------
    def run( self ):
        """
        called when thread is started from gt its call should be considered as from ht
        """
#         self.parameters.init_from_helper()

#         self.scheduled_event_list   = AppGlobal.scheduled_event_list
#         id = threading.get_ident()

#         msg = f"helper run  "
#         AppGlobal.log_if_wrong_thread( threading.get_ident(), msg = msg, main = False  )

#         self.logger.debug(  msg )

# #        #rint( "==============================================" )
#         self.polling()   # may mach name in gui thread
        self.db_objects                = db_objects.DBObjects( self.parameters.db_file_name )

        self._polling_()
        pass
        #print( "ht start ")

    # ------------------------------------------------
    def _polling_(self):
        """
        started from gui thread as beginning of new thread
        this is an infinite loop monitoring the queue
        actions based on queue_to_helper and run_event
        application purpose is the device polling where we monitor/record the devices

        """
        # comment out after testing
        msg    =  "HelperThread _polling_()"
        AppGlobal.log_if_wrong_thread( threading.get_ident(), msg = msg, main = False )

        while True:
            #rint( "while True" )
            try:

                ( action, function, function_args ) = self._rec_from_queue_()
                if action != "":
                    self.logger.debug(  "helper_thread.polling() queue: " + action + " " + str( function ) + " " + str( function_args) )  # ?? comment out

                if action == "call":
                    #rint( "ht making call" )
                    sys.stdout.flush()
                    self.controller.helper_task_active  = True
                    function( *function_args )
                    self.logger.debug(  "smart_plug_helper.polling() return running helper loop "  )  # ?? comment out

                    #self.print_helper_label( "return running helper loop " )
                    self.controller.helper_task_active  = False    # do we maintain this, or move to helper -- looks like not used, app global better location
                    self.app_state.helper_running       = False    # looks like dup of above -- change to busy or runnin function.....

                    # let gui ... know we are done
                    post_action    = "call"
                    post_function  = self.controller.ht_state_change
                    #post_function  = self.controller.null_method
                    self._post_to_queue_(post_action, post_function, (False, ))

                if action == "stop":
                    # seems to be working fine
                    msg    =  "helper got stop in the queue -- end thread with return "
                    print( msg )
                    # AppGlobal.what_thread( threading.get_ident(), msg, 50  )
                    self.controller.helper_task_active  = False
                    return   # this will kill the thread --- think it is the return which should end the loop ?  or do we need a break -- we seem to keep looping
                    msg    =  "helper got stop in the queue -- ran thru return "
                    # AppGlobal.what_thread( threading.get_ident(), msg, 50  )

            # must catch all exceptions if we do not want polling to stop ... but maybe we do

            except app_global.UserCancel as he:
                pass    # user has cancled
                msg   = f"HelperThread  user cancled: {he}"

                print(  f"see log: {msg}" )
                self.gui_write( msg )

                self.app_state.cancel_flag        = False
                self.logger.error( msg,  exc_info = True )

            except Exception as he:
                #self.logger.info( "schedule_me_helper.HelperThread threw exception from " + he.msg )        # info debug... !! also info to msg area
                msg   = f"HelperThread threw exception: {he}"
                print(  f"see log: {msg}" )
                self.logger.error( msg,  exc_info = True )        # info debug...
                raise     # comment out when not debugging
            time.sleep( self.ht_poll_delta_t )  # ok here since it is the main pooling loop
            #self.logger.debug(  "HealperThread.polling() done with sleep " )
            #rint("helper polling")
            #ime.sleep( .1 )             # debug
        return

    # ------------------------------------------------
    def end_helper( self, ):
        """
        !! never needed or used ??
        a function to interrupt the help thread and go back to polling
        function called to end the helper subroutine
        if another function is running posting for this
        will cause it to throw an exception
        ends request_to_pause
        helper thread only
        call ht
        """
        self.print_helper_label( "end_helper " )
        #self.helper_label.config( text = "test_ports testing ports" )
        #self.print_info_string( "Helper Stopped" )
        #self.post_to_queue(  "info", None, ( "Helper Stopped", ) )
        msg = "end_helper( ) Helper interrupted"
        self.print_info_string( msg )    # rec send info
        self.logger.debug( msg )

 # --------------------------------------------------
    def _rec_from_queue_( self, ):
        """
        call from helper only, is looked for in the method run when polling.
        get item from queue if any.
        ret: tuple as below, if action == "" then nothing rec from queue
        ( action, function, function_args ) = self._rec_from_queue_()
        call ht
        """
        # why not use Queue.empty() bool function ?? in code below have wrong kind of exception
        # got error  except Queue.Empty:  AttributeError: 'NoneType' object has no attribute 'Empty'
        is_empty = False
        try:

            action, function, function_args   = self.queue_to_helper.get_nowait()
        except queue.Empty:
            action          = ""
            function        = None
            function_args   = None
            is_empty        = True

        if not( is_empty ):
            self.logger.debug( "in helper, rec_from_queue  " + action + " " + str( function ) + " " + str( function_args) )  # ?? comment out

        return ( action, function, function_args )

    # --------------------------------------------------
    def _post_to_queue_( self, action, function, args ):
        """
        post args to the queue to the controller
        call from: helper thread
        args: action=string, function=a function, args=tuple of arguments to function
        ret: zip

        example uses:
        self.post_to_queue(  action, function, args_as_tuple )
        self.post_to_queue(  action, function, arg_a_string )   # ok will be converted to a tuple
        self.post_to_queue(  "call", function, args_as_tuple )
        self.post_to_queue(  "rec",  None, ( "print me", ) )    # rec send info
        self.post_to_queue(  "send", None, ( "print me", ) )
        self.post_to_queue(  "info", None, ( "print me", ) )    # rec send info
        """
        # convert to make calling more flexible esp
        if type( args ) is str:
            args = ( args, )

        loop_flag          = True
        ix_queue_max       = 100
        self.ix_queue      = 0   # why instance
        # self.release_gui_for( .0 )
        while loop_flag:
            loop_flag      = False
            self.ix_queue  += 1
            try:
                #print( "try posting " + action + " args= " + str( args ) )
                self.queue_fr_helper.put_nowait( ( action, function, args ) )
            except queue.Full:
                # try again but give polling a chance to catch up
                print( "helper queue full looping" )
                self.logger.error( "helper post_to_queue()  queue full looping: " +str( action ) )
                # protect against infinit loop if queue is not emptied
                if self.ix_queue > ix_queue_max:
                    #print "too much queue looping"
                    self.logger.error( "helper post_to_queue() too much queue looping: " +str( action )  )
                    pass
                else:
                    loop_flag = True
                    time.sleep( self.parameters.ht_queue_sleep )   # ??

    # ----------------------------------------------
    def print_ten_times( self ):
        """
        for thread testing
        call from ht through queue
        """
        for ix in range( 10 ):
            print( f"helper_thread.print_ten_times  {ix}")
            time.sleep( 1 )

    # ----------------------------------------------
    def write_file_dups( self,  ):
        """
        used as callback from gui button
        call ht  normally via queue
        """
        file_name   =  "file_name.txt"  #  !! move to parameters

        AppGlobal.gui.do_clear_button( None )
        msg   = "---- write_file_dups  ( {file_name} ) ----"
        AppGlobal.gui.write_gui( msg )
        self.db_objects.write_file_dups( file_name )
        msg   = "---- write_file_dups complete ----"
        AppGlobal.gui.write_gui( msg )

    # ----------------------------------------------
    def define_table_dups( self, ignored_arg  ):
    #def define_table_dups( self, ignored_arg  ):
        """
        define the db table for dups
        call ht  normally via queue
        """
        #rint( "helper_thread.define_table_dups")
        self.db_objects.define_table_dups(  allow_drop = True )
        #rint( "\n################# and we are done define_table_dups ############## ")
        self.db_objects.commit( for_sure = True )
        msg            = "\n---- Define Empty DB... Done ----"
        self.gui_write(  msg, )

    # ----------------------------------------------
    def gui_write( self, msg,   ):
        """
        what it says
        could inline as one line ??
        self.gui_write( msg )
        """
        post_action    = "call"
        post_function  = self.controller.gui.write_gui
        self._post_to_queue_( post_action, post_function, ( msg, ) )

    # ----------------------------------------------
    def explore_dir_keeps( self, starting_dir_list,   ):
        """
        later get a list and loop over it with explore_dir -- this for keeps
        """
        #rint( "---- Explore Directories to Keep Files ----" )
        a_explore_args     = explore_args.ExploreArgs( self.db_objects )
        a_explore_args.set_for_keep(  )

        self.app_state.ix_explore_dir      = 0
        self.app_state.ix_explore_file     = 0

        for i_starting_dir in starting_dir_list:
            self.explore_dir( i_starting_dir, 0, a_explore_args  )

        print( "\n################# and we are done explore_dir_keeps ############## ")

        msg     = f"total files found for keep {self.app_state.ix_explore_file}"
        self.gui_write(  msg )

        msg     = "\n---- Explore (possible) Duplicate Files... Done ----"
        self.gui_write(  msg )

    # ----------------------------------------------
    def explore_dir_dups( self, starting_dir_list,   ):
        """
        later get a list and loop over it with explore_dir
        """
        a_explore_args     = explore_args.ExploreArgs( self.db_objects )
        a_explore_args.set_for_dups(   )

        self.app_state.ix_explore_dir      = 0
        self.app_state.ix_explore_file     = 0

        for i_starting_dir in starting_dir_list:

            self.explore_dir( i_starting_dir, 0, a_explore_args  )

        #rint( "\n################# and we are done explore_dir_dups ############## ")

        msg   = "\n---- Explore (possible) Duplicate Files... Done ----"
        self.gui_write(  msg, )

    # ----------------------------------------------
    def explore_dir( self, starting_dir, dir_depth, explore_args  ):
        """
        could collect files in a list and process as a batch at the end
        probably more efficient but for now one at a time

        explore and list files in dir and recurssive to sub dirs
        starting_dir = name of dir we start from
        dir_depth    = depth of starting_dir, 0 for initial call
        additional args   current depth
                           filter
        ( self, starting_dir )
        """

        """
        call from backup thread only
        run in its own thread
        so what is new, what is defect
        this is really the backup thread called from run backup
        ** move a function into backup_thread object DirectoryBackup  was Pycopytree
        !! this logic is getting a little complicated
        src     is the string name of source directory
        dst     is the string name of destination directory
        """
        if self.app_state.pause_flag:
             time.sleep( self.parameters.ht_pause_sleep )

        if self.app_state.cancel_flag:
             msg = "user cancel"
             raise app_global.UserCancel( msg )

        starting_dir    = starting_dir.replace( "\\", "/" )    # normalize win/linux names
        new_dir_depth   = dir_depth + 1
        names           = os.listdir( starting_dir )

        for i_name in names:
            # file from / file to
            i_name        = i_name.replace( "\\", "/" )
            i_full_name   = os.path.join( starting_dir, i_name )   ## ?? just default to / why not and remove next
            i_full_name   = i_full_name.replace( "\\", "/" )
            i_file_info   = FileInfo( file_name = i_name, path_name = starting_dir, full_file_name = i_full_name )

            # could have pause here too #
            if self.app_state.cancel_flag:
                 msg = "user cancel"
                 raise app_global.UserCancel( msg )

            if self.app_state.pause_flag:
                time.sleep( self.parameters.ht_pause_sleep )

            if os.path.isdir( i_full_name ):
                msg     = ( f"os.path.isdir self.app_state.ix_explore_dir = {self.app_state.ix_explore_dir} new_dir_depth = {new_dir_depth}" +
                            f"    explore_args.max_dir_depth = {explore_args.max_dir_depth}" )
                print( msg )

                if ( explore_args.max_dir_depth == -1  ) or  ( explore_args.max_dir_depth  >  new_dir_depth  ):   # may be more efficient placement of this so called once
                    self.app_state.ix_explore_dir      += 1    # or one for file, one for dir, and one for error better ??
                    if explore_args.df( i_full_name ):   # the filter for dir
                        msg     = f"\nmaking recursive call {i_full_name} {new_dir_depth}"
                        print( msg )
                        self.explore_dir( i_full_name, new_dir_depth, explore_args )
                    else:
                        msg     = f"\nhit false on dir filter df  {i_full_name} "
                        print( msg )
                else:
                    msg         = f"\nhit max dir depth {i_full_name} {new_dir_depth} "
                    print( msg )
                continue
            #import pdb; pdb.set_trace()

            if explore_args.ff( i_name, i_full_name ):
                self.app_state.ix_explore_file      += 1    # or one for file, one for dir, and one for error better ?? filteerd or not
                # then it is a file --- still need filter, let it come from app state
                #rint( f"file found {i_full_name} / {i_name} ")
                msg   = f"file found {self.app_state.ix_explore_file} {i_full_name}"
                self.gui_write( msg )

                if explore_args.process_item_fun is not None:
                   explore_args.process_item_fun( i_file_info )   # for possible dups adds to db
                else:
                   pass # !! but this is an error
            else:
                msg   = f"file found but filtered out {self.app_state.ix_explore_file} {i_full_name}"
                AppGlobal.logger.debug( msg )
                self.gui_write( msg + "\n" )

        AppGlobal.db_objects.commit( for_sure = True )  # !! still need to make sure used in very case inc user cancel.

    # ----------------------------------------------
    def template( self, file_name ):
       """
       what it says, read

       """
       pass

    # ----------------------------------------------


    # ----------------------------------------------
    def summary_to_str( self,   ):
       """
       summarse what has happened

       """
       a_string   = f"{1}"
       a_string   = f"{a_string}\n{1}"
       return a_string


# =================================================
if __name__ == "__main__":

    import  file_finder
    file_finder.main( )


# =================== eof ==============================

