# -"*- codin"g: utf-8" -*-

"""
parameters for file_finder



"""

import logging
import sys
import os

# ------ local ------

#import file_filters
from   app_global import AppGlobal
import file_filters

#-------------------------------------------
class Parameters( object ):
    """
    manages parameter values use like ini file
    a struct but with a few smarts
    """

    # -------
    def choose_mode( self ):
        """
        just choose one of the modes below
        """

        # ---- ---------- choose ---------
#        self.russ_1_mode()
        #self.mode_test_1()
#        self.test_photo_db_1()

        #self.mode_test_for_dev_1()
        #self.mode_decade_scan(  )
        # self.mode_photo_raw()
        #self.test_photo_db_1()
        return

    # ---- --->> Methods to set up modes

        # -------
    def mode_photo_raw( self ):
        """
        right now just to scan photo raw
        """
        self.mode               = "mode_photo_raw"
        self.db_file_name       = r"photo_raw_db.db"

        # --------------------- dup finding related

        # add check to make sure all are directories
        self.keep_dir_depth         = -1
        self.dup_dir_depth          = -1

        # for testing in dev -- more directions needed for setup
        self.keep_dir_list          = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\keeps", ]

        # recreate from D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_template
        self.dup_dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_test", ]
        self.dup_dir_list           = [ r"D:\PhotosRaw", ]
        self.set_filter_for_images()

        self.dup_mode                    = "move"   #  simulate delete move

        self.logging_level               = logging.INFO   #  DEBUG  iNFO 20

    # -------
    def mode_test_for_dev_1( self ):
        """
        a test for use in development
        """
        self.mode               = "mode_test_for_dev_1"

        # --------------------- dup finding related

        # add check to make sure all are directories
        self.keep_dir_depth         = -1
        self.dup_dir_depth          = -1

        # for testing in dev -- more directions needed for setup
        self.keep_dir_list          = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\keeps", ]

        # recreate from D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_template
        self.dup_dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_test", ]
        self.dup_dir_list           = [ r"D:\PhotoDB\19", ]


        # --------------- file filter, this is a bit more complex
        # a_filter                         = file_filters.AFileFilter()   # the object not the function
        # a_filter.filter_name             = "AFileFilter modified"
        # a_filter.filter_description      = " "

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude ?? use for True False and other functions ??
        # a_filter.extensions              = ["png", "jpg" ]   # make a list to accept or exclude
        # a_filter.extensions              = ["sss", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # #a_filter.extensions              = ["txt", "xls" ]   # make a list to accept or exclude

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude
        # a_filter.filter_dir_names        = None    # make a list for included or excluded
        # a_filter.filter_true_false_bool  = True   # for the function
        # a_filter.lower_ext_bool          = True    # True lower case the extensions

        # a_filter.filter_file_function    = a_filter.file_filter_ext_function     # set the function

        # self.file_filter_object          = a_filter

        self.dup_mode                    = "move"   #  simulate delete move

        self.logging_level               = logging.INFO   #  DEBUG  iNFO 20

        msg    = f"filter info {self.file_filter_object}"
        print( msg )  # but this is already in gui

    # -------
    def mode_decade_scan( self ):
        """
        a test for use in development
        """
        self.mode               = "mode_decade_scan"

        # --------------------- dup finding related

        # add check to make sure all are directories
        self.keep_dir_depth         = -1
        self.dup_dir_depth          = -1
                # for testing in dev -- more directions needed for setup
        self.keep_dir_list          = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\keeps", ]

        # recreate from D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_template
        self.dup_dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_test", ]
        self.dup_dir_list           = [ r"D:\PhotosRaw\04",
                                        r"D:\PhotosRaw\05",
                                        r"D:\PhotosRaw\06",
                                        r"D:\PhotosRaw\2010",
                                        r"D:\PhotosRaw\2011",
                                        r"D:\PhotosRaw\2012",
                                        r"D:\PhotosRaw\2013",
                                        r"D:\PhotosRaw\2014",
                                        r"D:\PhotosRaw\2015", ]

        self.set_filter_for_images()


    # -------
    def mode_test_photo_db_1( self ):
        """
        we are actually going to try this out
        """
        self.mode               = "test_photo_db_1"

        self.logging_level      = logging.DEBUG

        # add check to make sure all are directories
        # add check to make sure all are directories
        self.keep_dir_depth         = 2
        self.dup_dir_depth          = 2

        self.dup_dir_list           = [ r"D:\PhotosRaw\2021\from_phone_june21\pony" ]

        # recreate from D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_template
        self.keep_dir_list          = [ r"D:\PhotosRaw\2021\all_from_phone_cut_july" ]

        #----------------------------
        self.dup_dir_list           = [ r"D:\PhotosRaw\2021\from_phone_june21\pressure_wash"]

        self.keep_dir_list          = [ r"D:\PhotosRaw\2021\all_from_phone_cut_july" ]

        # ---- directory filters ... need to fix for rshlib
        self.dup_dir_filter      = df_all

        self.keep_dir_filter     = df_all

        # ---- file filters
        self.dup_file_filter     = ff_all

        self.keep_file_filter    = ff_all

        self.dup_mode            = "simulate"   #  ue, do not actually do deletes, just print message False, delete for real

    # -------
    def mode_russ_1_mode( self ):
        """
        first mode for smithers
        untested, prob bad
        """
        self.mode               = "mode_russ_1_mode"

        self.logging_level      = logging.DEBUG

        # add check to make sure all are directories
        self.dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\fileprocessor",
                                    r"D:\Russ\0000\python00\python3\_projects\fileprocessor\delete", r"D:\Russ\2019" ]

        self.dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\mushrooms\Ver5",
                                    r"D:\Russ\0000\python00\python3\_projects\mushrooms\Ver2",
                                    r"D:\Russ\0000\python00\python3\_projects\mushrooms\Ver3",
                                    r"D:\Russ\0000\python00\python3\_projects\mushrooms\Ver3a",
                                    r"D:\Russ\0000\python00\python3\_projects\mushrooms\Ver4",       ]

        self.file_list_fn       = r"files_out.txt"
        self.file_dup_list_fn   = r"dup_files.txt"
        self.dup_list_format    = None                     # work on this when have more ideas


    #
    def set_filter_for_images( self ):
        """
        use as part of a mode for easier filter setup
        set up a photo filter, by extension, ignore case, jpg, gif, png bmp.... but no mov....
        easy but so you do not have to remember
        needs testing
        """
        a_filter                         = file_filters.FFExtList( [ "png", "jpg", "gif", "bmp" ] )   # the filter object not the function is inside it
        a_filter.filter_name             = "Filter for images"
        self.file_filter_object          = a_filter

     # -------
    def set_filter_for_xxx():
        """
        for something else
        """
        pass
        # --------------- file filter, this is a bit more complex
        a_filter                         = file_filters.FFExtList( [ "png", "jpg" ] )   # the object not the function
        a_filter.filter_name             = "Filter for images"
        # a_filter.filter_description      = " "

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude ?? use for True False and other functions ??
        # a_filter.extensions              = ["png", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # a_filter.extensions              = ["sss", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # #a_filter.extensions              = ["txt", "xls" ]   # make a list to accept or exclude

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude
        # a_filter.filter_dir_names        = None    # make a list for included or excluded
        # a_filter.filter_true_false_bool  = True   # for the function
        # a_filter.lower_ext_bool          = True    # True lower case the extensions

        # a_filter.filter_file_function    = a_filter.file_filter_ext_function     # set the function

        # self.file_filter_object          = a_filter





# ---- methods not changed for different modes

# ----------------- tweaks changes for all modes for an os or computer name, to be used with other modes

    def __init__(self,   ):

        self.controller            = AppGlobal.controller    # save a link to the controller not actually used now
        AppGlobal.parameters       = self

        self.file_filter_function  = None

        # ---------  os platform... ----------------
        self.default_config()
        self.os_tweaks( )
        self.computer_name_tweaks()
        self.choose_mode()

    # -------
    def os_tweaks( self ):
        """
        tweak the default settings of "default_ _mode"
        for particular operating systems
        you may need to mess with this based on your os setup
        """
        if  self.os_win:
            pass
            #self.icon               = r"./clipboard_b.ico"    #  greenhouse this has issues on rasPi
            #self.icon              = None                    #  default gui icon

        else:
            pass

    # -------
    def computer_name_tweaks( self ):
        """
        this is an subroutine to tweak the default settings of "default_mode"
        for particular computers.  Put in settings for you computer if you wish
        these are for my computers, add what you want ( or nothing ) for your computes
        """
        print(self.computername, flush=True)

        if self.computername == "smithersxx":
            self.win_geometry       = '1250x1000+20+20'      # width x height position
            self.ex_editor          =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10 smithers

        elif self.computername == "millhousexx":
            self.ex_editor          =  r"C:\apps\Notepad++\notepad++.exe"    # russ win 10 millhouse
            self.win_geometry       = '1000x700+250+5'          # width x height position
#            self.pylogging_fn       = "millhouse_clipboard.py_log"   # file name for the python logging
#            self.snip_file_fn       = r"C:\Russ\0000\python00\python3\_projects\clipboard\Ver3\snips_file_auto.txt"
#            # need to associate with extension -- say a dict
#            self.snip_file_command  = r"C:\apps\Notepad++\notepad++.exe"  #russwin10   !! implement


    # -------
    def default_config( self ):

        self.mode              = "default_config"
        our_os = sys.platform
#        print( "our_os is ", our_os )

        #if our_os == "linux" or our_os == "linux2"  "darwin":

        if our_os == "win32":
            self.os_win = True     # right now windows and everything else
        else:
            self.os_win = False

        self.platform           = our_os    # sometimes it matters which os

        self.computername       = ( str( os.getenv( "COMPUTERNAME" ) ) ).lower()

        # ---- appearance
        self.title              = "File Finder for Dups"   # window title  !! drop for name version

        self.icon               = r"clipboard_c.ico"
        self.icon               = r"broom_edit_2.ico"    # do we need to convert to ico -- or down sample

        self.id_color           = "yellow"                # ?? not implementd

        self.win_geometry       = '1500x800+20+20'        # width x height position
        self.win_geometry       = '900x600+700+230'       # width x height position  x, y
        self.win_geometry       = '1400x1000+20+20'

        self.default_scroll     = True
        self.max_lines          = 2_000

        # ---- logging
        self.pylogging_fn       = "file_finder.py_log"   # file name for the python logging
        self.logging_level      = logging.DEBUG          # logging level DEBUG will log all catured text !
        self.logging_level      = logging.INFO
        self.logger_id          = "file_finder"          # id in logging file

        self.gui_text_log_fn    = False    # "gui_text.log"       # a file name or something false
        self.log_gui_text       = False    # True or false to log text
        self.log_gui_text_level = 10       # logging level for above

        # ------------- file names -------------------
        # this is the name of a program: its executable with path inof.
        # to be used in opening an external editor
        self.ex_editor         =  r"D:\apps\Notepad++\notepad++.exe"    # russ win 10

        #  example use as if self.controller.parameters.os_win:

        self.db_file_name       = r"file_db.db"    # put on ram drive for speed  or None for in memory db
        self.db_file_name       = None

        self.help_fn            = "help.txt"
        self.help_fn            = "https://opencircuits.com/index.php?title=Delete_Duplicates_Help_File"

        # ---------------------thread related ------------------------------
        #self.queue_sleep         = 2          # time for backup to sleep if queue fills
        self.gt_queue_sleep      = 2          # time in seconds  -- this is new name aqueue_sleep is on its way  gt is gui therad
        #gt_poll_delta_t
        self.gt_poll_delta_t     = 200        # how often wee poll for clip changes, in ms ?
        self.queue_length        = 50
        self.ht_poll_delta_t     = .200        # how often wee poll for clip changes, in sec
        self.ht_queue_sleep      = 2          # time in seconds  -- this is new name aqueue_sleep is on its way  gt is gui therad
        self.ht_pause_sleep      = 1

        self.ix_queue_max        = 10         # number of times queue sleep is allowed to loop before error

        # --------------------- dup finding related

        # add check to make sure all are directories

        self.dup_dir_depth          = -1
        self.keep_dir_depth         = -1

        # for testing in dev -- more directions needed for setup
        self.keep_dir_list          = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\keeps" ]

        # recreate from D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_template
        self.dup_dir_list           = [ r"D:\Russ\0000\python00\python3\_projects\file_finder\files_for_testing\dups_test" ]


        self.set_filter_for_images()


        # self.dir_filter      = df_all  !! later set up as object like file filter

        # --------------- file filter, this is a bit more complex
        # a_filter                         = file_filters.AFileFilter()   # the object not the function
        # a_filter.filter_name             = "AFileFilter modified"
        # a_filter.filter_description      = " "

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude ?? use for True False and other functions ??
        # a_filter.extensions              = ["png", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # a_filter.extensions              = ["sss", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # #a_filter.extensions              = ["txt", "xls" ]   # make a list to accept or exclude

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude
        # a_filter.filter_dir_names        = None    # make a list for included or excluded
        # a_filter.filter_true_false_bool  = True   # for the function
        # a_filter.lower_ext_bool          = True    # True lower case the extensions

        # a_filter.filter_file_function    = a_filter.file_filter_ext_function     # set the function

        # self.file_filter_object          = a_filter

        # #================ new
        # filter_obj          = file_filters.FFExtList( ["txt", "py", "jpg" ], include_true = True, use_lower = True )
        # # a_filter_function   = filter_obj.check_file
        # self.file_filter_object  = filter_obj

        # # ================= end new


        # now the same for directories
        a_filter                         = file_filters.DFAll()   # the object not the function
        # a_filter.filter_name             = "AFileFilter modified for directories "
        # a_filter.filter_description      = " "

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude ?? use for True False and other functions ??
        # a_filter.extensions              = ["png", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # a_filter.extensions              = ["sss", "jpg" ]   # make a list to accept or exclude see a_filter.filter_include
        # a_filter.extensions              = ["txt", "xls" ]   # make a list to accept or exclude

        # a_filter.filter_include          = True    # True, include files with this extension, False exclude
        # a_filter.filter_dir_names        = None    # make a list for inclued or exclued
        # a_filter.filter_true_false_bool  = True   # for the funtion
        # a_filter.lower_ext_bool          = True    # True lower case the extensions


        # a_filter.dir_filter_function     =  a_filter.dir_filter_all_function

        self.dir_filter_object           = a_filter

        self.dups_dir_depth              = -1
        self.keep_dir_depth              = -1

        self.dup_mode                    = "simulate"        # delete simulate move    # !! not implemented or tested
        #self.simulate                    = True   # True, do not actually do deletes, just print message
        self.del_directory               = "delete_me"



# =================================================
if __name__ == "__main__":
    import  file_finder
    file_finder.main( )

# =================== eof ==============================





