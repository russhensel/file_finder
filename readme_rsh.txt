this file should be D:\Russ\0000\python00\python3\_projects\file_finder\readme_rsh.txt

use to find duplicate files and delete ( or something else? ) them
used to explore directory trees

==============
this readme
    *>text    D:\Russ\0000\python00\python3\_projects\file_finder\readme_rsh.txt
     is for the file_finder

    this file is largely developer notes, much of which may be scratch items

see also the more user oriented help file
            *>text    help.txt


This (file_finder.py) is an application to find and list files to an output text file

    the main idea at first is to located duplicate files to delete.
    directories are listed in two groups, the keeps and the (possible) dups or duplicates

====================== status current Ver 8==============

file found but   --- helper


ver7 runs -- seems to work with new filter objects, dir filter is just all dirs, file filete is better
             names of filter more or less suck .... so save as checkpoint and fix in ver7

             simulate works light testing
             delete works light testing
             move works light testing
             filters work light testing on extension filter which may be all I need for now



======== ver9  ========
( move undone up by versions )

status:  good ready to move on with enhance in ver10

!! expand database for date....  file date, size, tiff data
!! batch commits


!! add sort prior to file write
!! make sort adjustable from the gui

!! change name to file or disk cleanup or file explorer
*! other options other than delete, inc move

*! clean up

?? add display of running on -- why / why not

!! ht may need loop limit
*! dir depth = 0 not working ... but think works as 1 so counting starts at one
!! find where "file"  is created and get rid of it ... seems to be comming from db objects, possible not closed ??? more research
!! add edit of saved file


======== ver8  ========
( move undone up by versions )



!! expand database for date....
!! add sort prior to file write
!! make sort adjustable from the gui
** write a file list
?? can use a dict instead of a db, should I code that??  -- not for now add more db functionality
xx improve setup of file filter to show only options that apply to selected filter

!! change name to file or disk cleanup or file explorer
 ..
** only one dir and file filter makes sense
*! other options other than delete, inc move

*! clean up
** check on filters ... seem ok, more testing would be good
?? add display of running on

!! ht may need loop limit
*! dir depth = 0 not working ... but think works as 1 so counting starts at one


======== ver7  ========

status: checkpoint as all seems to be working


** move not working why ... was not finished, finish  more test perhaps
!! change name to file or disk cleanup
** improve filter names ....

*! other options other than delete, inc move

*! clean up
** check on filters ... seem ok, more testing would be good
?? add display of running on

!! ht may need loop limit
*! dir depth = 0 not working ... but think works as 1 so counting starts at one


======== ver6  ========

was not deleting dup but keeps as saved  this is NG

!! change name to file or disk cleanup
** file filters as a class  -- this is the reason for change in version
*! only one dir and file filter makes sense
!! other options other than delete, inc move
*! get rid of old code
*! clean up
** check on filters
!! add display of running on

** show setup
** show progress in gui -- of cours always want more
** fix stop to end ht
!! ht may need loop limit
!! dir depth = 0 not working
** auto scroll not working

======== ver5  ========

now not working

!! file filters as a class
!! only one dir and file filter makes sense
!! other options other than delete, inc move
*! get rid of old code
*! clean up
!! check on filters

** show setup
** show progress in gui -- of cours always want more
!! fix stop to end ht
!! ht may need loop limit
** dir depth = 0 not working
** auto scroll not working

======== ver4  ========

*! get rid of old code
*! clean up
!! check on filters
** get second thread running

======== ver3  ========

*! get rid of old code
*! clean up
!! check on filters
** make test dir and test


======== ver2  ========
** use db for new delete plan



============== ideas of things to add ?? =================

?? move files
?? write delte batch ....

!! make virtual db -- would need to create one place and pass around probably in db objects ... ends when connection gone
** add pause and stop
** move to second thread
** progress to gui
!! make file filters objects so can support filtering and help and instance variables --- look for an implementations sowewhere

!! add delete empty dir
!! add delete dir by name ... a list

** add file filters  -- some in code, may be working,   functions in parameters

?? validate directories
** mark up output to indicated which files might be deleted
?? add button to access this file
** put messages in the text areas
** log some -more- stuff
**! improve messages
** clean up uneeded hints
** fix sort more
** title for top buttons -- put color there ?  --- made new utility class for this
** add directories to expand -- all sub directories to some level



============== Environment =================

Application developed in the standard python 3.6 environment or up ( uses f"" so do not go < 3.6 )
Windows 10 Pro, but should work on any Python 3.6 environment and up
GUI in tkinter
tasks tend to be quick so this app is single threaded -- moving to multithreaded
Author:    Russ Hensel
russ-hensel (russ_hensel)
    *>url  https://github.com/russ-hensel






















