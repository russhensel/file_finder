# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 13:11:34 2019

@author: Russ
"""

import file_finder

# ------------------ helper ----------------------

extensions_1   = [ "jpg", "txt"]

a_file_list    = [
r"D:/Russ/2019/drafts_2019.txt",
r"D:/Russ/2019/delete_me.txt",
r"D:/Russ/2019/aac_delete.txt",
r"D:/Russ/2019/2019Computer.ods",
r"D:/Russ/2019/76ed7275014fc6a076bd465a73f33844a810.pdf",
r"D:/Russ/2019/tp_p72_hmm_en.pdf",
r"D:/Russ/2019/ThinkPad P72 Mobile Workstation max graphics.pdf",
r"D:/Russ/2019/ThinkPad P72 Mobile Workstation _ 2TDisk32GMem.pdf",
r"D:/Russ/2019/ThinkPad_P72_Platform_Specifications.pdf",
r"D:/Russ/2019/WBUR Donation Page.pdf",
r"D:/Russ/2019/ThinkPad P72 Mobile Workstation _ 20MBCTO1WWENUS0 _ Lenovo US.pdf",
r"D:/Russ/2019/F17USXKHD7U1BVP.LARGE.jpg",
r"D:/Russ/2019/dan_note.txt",
r"/D:/Russ/2019/NZ_study_guide.txt",
r"D:/Russ/2019/MediaId.txt",
r"D:/Russ/2019/Smithers_My_Places.kmz",
]

# ----------------------------------------
def ex_file_filter():
    print( """
    ================ ex_file_filter() ===============
    """ )
    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".jpg", ".txt"]
    filter_object.add_extensions( a_few_extensions )

    for i_file_name in a_file_list:

        is_ok    = filter_object.check_file( i_file_name   )
        print(f"for >>{i_file_name}<<>>{is_ok}")

    filter_object.add_extensions( [".txt"] )

#ex_file_filter()


# ----------------------------------------
def ex_file_filter_for_parmeters_1():
    print( """
    =============================== ex_file_filter_for_parmeters_1() ===============
    """ )
    a_function        = None


    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".jpg", ".txt"]
    filter_object.add_extensions( a_few_extensions )

    a_function        = filter_object.check_file

    for i_file_name in a_file_list:

        is_ok    = a_function( i_file_name   )
        print(f"for >>{i_file_name}<<>>{is_ok}")

    filter_object.add_extensions( [".txt"] )

#ex_file_filter_for_parmeters_1()

# self.filter_function   = FF_AllFiles().check_file


# ----------------------------------------
def ex_file_filter_for_parmeters_2():
    print( """
    =============================== ex_file_filter_for_parmeters_2() a style for compounding functions ===============
    """ )
    a_function        = None


    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".jpg", ".txt"]
    filter_object.add_extensions( a_few_extensions )
    a_function_1        = filter_object.check_file

    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".jpg", ]
    filter_object.add_extensions( a_few_extensions )
    a_function_2        = filter_object.check_file

    def my_filter( ffn ):
        # choose 1
#        return( a_function_1( ffn ) and a_function_2( ffn ))
        return( a_function_1( ffn ) and not a_function_2( ffn ))

    for i_file_name in a_file_list:

        is_ok    = my_filter( i_file_name   )
        print(f"for >>{i_file_name}<<>>{is_ok}")

    filter_object.add_extensions( [".txt"] )

#ex_file_filter_for_parmeters_2()

# self.filter_function   = FF_AllFiles().check_file

# ----------------------------------------
def ex_test_create():
    print( """
    =============================== ex_file_filter_for_parmeters_2() a style for compounding functions ===============
    """ )
    # ------------------ make a filter for...

    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".py", ".txt"]
    filter_object.add_extensions( a_few_extensions )
    a_function_1        = filter_object.check_file

    filter_object     = file_finder.FFByExtension( )
    a_few_extensions  = [ ".txt", ]
    filter_object.add_extensions( a_few_extensions )
    a_function_2        = filter_object.check_file

    print("iran")

ex_test_create()

# ----------------------------------------
class TestClass( object ):
    """
    manages parameter values use like ini file
    a struct but with a few smarts
    """
    def __init__(self,   ):

        filter_object     = file_finder.FFByExtension( )
        a_few_extensions  = [ ".py", ".txt"]
        filter_object.add_extensions( a_few_extensions )
        a_function_1        = filter_object.check_file

        filter_object     = file_finder.FFByExtension( )
        a_few_extensions  = [ ".txt", ]
        filter_object.add_extensions( a_few_extensions )
        a_function_2        = filter_object.check_file

    filter_objectx     = file_finder.FFByExtension( )
    a_few_extensionsx  = [ ".py", ".txt"]
    filter_objectx.add_extensions( a_few_extensionsx )
    a_function_1x        = filter_objectx.check_file




# ----------------------------------------
def ex_test_class():
    print( """
    =============================== ex_test_class() a style for compounding functions ===============
    """ )
    a_class = TestClass()

#ex_test_class()



# ----------------------------------------